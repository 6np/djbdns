#!/bin/bash

##############################################
#
# Switching DNS from Bytemark to altenative provider
# 
# This script will move the BytemarkDNS upload script to 
# upload.BM in case you need to go back and create a new script 
# for the purposes of generating the DNS on this server and informing
# your upstream DNS servers (nameservers) of any changes. The upstream
# server can then request the updated DNS.
#
#
##############################################

#####
# Added here the option to pick out the nameservers
# from the template file now that we have changed them.
#####

echo 'We currently have these DNS servers listed as your Name servers:'

for option in $(sed -n -e 's/^\..*:.*:\(.*\):.*/\1/p' /etc/sympl/dns.d/tinydns.template.erb);
do
     ns+=($option)
     echo $option
done

read -e -p "Do you want to continue with these?[Y,n]" -i "Y" ans

if [ $ans == "n" ]; then
        echo 'Please input your nameservers, space separated with a new line to end:'
        read -a nameservers

elif [ $ans == "Y" ]; then
	# check for Bytemark nameservers. 
	
	if [[ ${ns[@]} == "a.ns.bytemark.co.uk b.ns.bytemark.co.uk c.ns.bytemark.co.uk" ]]; then
		echo 'Bytemark Nameservers found. This will break Bytemarks DNS upload so aborting'
		exit
	
	elif [[ -z ${ns[@]} ]]; then
		echo 'No nameservers found, aborting'
		exit
	
	else
	     echo 'Thank you. setting up your system.'
        
        fi	

else 
	echo 'Input not recognised. Please try again'
	exit
fi

echo "changing the Bytemark upload script..."

upload_script=/root/BytemarkDNS/upload
mv $upload_script $upload_script.BM

cat <<'EOF' > $upload_script
#!/bin/sh -eu

#
# Takes out tinydns fragments and joins them together.
#
datadir=/root/BytemarkDNS/data
dnsroot=/var/lib/tinydns/root
dnsdata=$dnsroot/data
dnsdatatmp=$(mktemp)

cat $datadir/*.txt > $dnsdatatmp
exec mv $dnsdatatmp $dnsdata
EOF

chmod +x $upload_script

echo "setting up the listener services"

for ip in $(hostname -i)
do
	systemctl enable tinydns@$ip
	systemctl enable axfrdns@$ip
	systemctl start tinydns@$ip
	systemctl start axfrdns@$ip
done

#
# We are now ready to send the DNS to the upstream servers
# but we need a mechanism to tell them that there are changes.
# We can use this script to let the know (NOTIFY).

# get IP's from nameservers
for ip in "${ns[@]}" 
do
	ips+=($(dig +short $ip))
done

echo "install a dependancy for the dnsruby script to work"
apt-get install -qqy ruby-dnsruby 
 
echo "setting up the NOTIFY script"
cat <<EOF > /usr/local/bin/sympl-dns-notify
#!/usr/bin/ruby

# Update these IP's with those of your service provider.
SECONDARIES=%w(${ips[@]}).freeze

require 'sympl/domains'
require 'dnsruby'

class Secondary

  attr_reader :resolver

  def initialize(server)
    @resolver = Dnsruby::Resolver.new(nameserver: server)
  end

  def notify(zones)
    zones.each{|z| send_notify(z) }
  end

  private

  def send_notify(zone)
    msg = Dnsruby::Message.new
    msg.header.rd = 1
    msg.header.aa = 1
    msg.header.opcode = Dnsruby::OpCode::Notify
    msg.add_question(zone, Dnsruby::Types::SOA)
    resolver.send_message(msg)
  rescue Dnsruby::ResolvError, Dnsruby::ResolvTimeout => err
    warn "*** #{zone}: #{err}"
  end
end

zones = Symbiosis::Domains.all.map(&:name)

SECONDARIES.each do |ip|
  secondary = Secondary.new(ip)
  secondary.notify(zones)
end
EOF

echo "set the NOTIFY script as executable"

chmod +x /usr/local/bin/sympl-dns-notify

# At the moment the acl for transfers is not set up so the 
# default deny will block any requests. we can allow the upstream
# servers with these lines

echo "adding access for upstream servers"

nu=3
ins="i"
acl=":allow"

for each in "${ips[@]}"
do
	sed -i "$nu$ins$each$acl" /etc/tinydns/axfrdns-acl
	let "nu++"
done

echo "we can set up a service to run this script"

cat <<'EOF' > /etc/systemd/system/sympl-dns-notify.service
[Unit]
Description=Notify secondary DNS servers about DNS updates

[Service]
Type=oneshot
User=root
ExecStart=/usr/local/bin/sympl-dns-notify
EOF

# We then need to know when to run this. We can set up a
# watcher on the tinydns data file so that when it changes
# the script is run

echo "watcher to run the NOTIFY script when the settings change."

cat <<'EOF' > /etc/systemd/system/sympl-dns-notify.path
[Unit]
Description=Watcher to send NOTIFY to upstream DNS servers

[Path]
PathChanged=/var/lib/tinydns/root/data.cdb

[Install]
WantedBy=multi-user.target
EOF

echo 'enable the watcher'

systemctl enable /etc/systemd/system/sympl-dns-notify.path

echo "Sympl firewall disallows DNS queries by default. We will let them in."

install -o sympl -g sympl /dev/null /etc/sympl/firewall/incoming.d/70-domain
sympl-firewall

echo "Now we can run the Sympl dns script"

sympl-dns-generate --upload
