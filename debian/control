Source: djbdns
Section: net
Priority: optional
Maintainer: Patrick J Cherry <patch@dominoid.net> 
Standards-Version: 4.3.0 
Build-Depends: debhelper (>= 10)

Package: djbdns
Architecture: any
Depends: ucspi-tcp-ipv6 | ucspi-tcp, ${shlibs:Depends}, ${misc:Depends}
Replaces: djbdns-doc, dbndns
Recommends: make
Description: collection of Domain Name System tools
 This package includes software for all the fundamental DNS operations:
 .
 DNS cache: finding addresses of Internet hosts.  When a browser wants to
 contact www.hotwired.com, it first asks a DNS cache, such as djbdns's
 dnscache, to find the IP address of www.hotwired.com.  Internet service
 providers run dnscache to find IP addresses requested by their customers.
 If you're running a home computer or a workstation, you can run your own
 dnscache to speed up your web browsing.
 .
 DNS server: publishing addresses of Internet hosts.  The IP address of
 www.hotwired.com is published by HotWired's DNS servers.  djbdns includes
 a general-purpose DNS server, tinydns; network administrators run tinydns
 to publish the IP addresses of their computers.  djbdns also includes
 special-purpose servers for publishing DNS walls and RBLs.
 .
 DNS client: talking to a DNS cache.  djbdns includes a DNS client C
 library and several command-line DNS client utilities.  Programmers use
 these tools to send requests to DNS caches.
 .
 djbdns also includes several DNS debugging tools, notably dnstrace, which
 administrators use to diagnose misconfigured remote servers.
 .
 See http://cr.yp.to/djbdns.html
 .
 This package has been patched to support the following records
   * AAAA
   * SRV
   * NAPTR
 as well as a number of other patches to requested by Debian developers and
 users.
